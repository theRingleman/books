#!/bin/bash

echo "Creating books network"
docker network create booksapi-net

docker rm -f booksapi

docker run --name="booksapi" \
    -p 1111:80 \
    -v $PWD/source/books/:/var/www/html/ \
    --network='books-net' \
    -d booksapi:latest
