<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Support\Facades\Artisan;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserAuthTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        Artisan::call('migrate');
    }

    public function tearDown(){
        Artisan::call('migrate:reset');
        parent::tearDown();
    }

    public function testLoginSuccess()
    {

        Passport::actingAs(
            factory(User::class)->create(),
            ['login']
        );

        $this->post('/api/auth/login');
        $this->assertAuthenticated();
    }

    public function testLoginFailure()
    {
        $response = $this->json('POST', '/api/auth/login', ['email' => 'test@test.te', 'password' => 'testerino']);
        $response
            ->assertStatus(401)
            ->assertJson(['message' => 'Unauthorized']);
        $this->assertGuest();
    }

    public function testSignUpSuccess()
    {
        $response = $this->json('POST', '/api/auth/signup', [
           'name' => 'Tester McTesterson',
           'email' => 'test@test.com',
           'password' => 'tester',
           'password_confirmation' => 'tester'
        ]);
        $response
            ->assertStatus(201)
            ->assertJson(['message' => 'Successfully created user!']);
    }

    public function testSignupFailure()
    {
        $user = factory(User::class)->create(['email' => 'test@test.te']);
        $response = $this->json('POST', '/api/auth/signup', [
            'name' => 'Tester McTesterson',
            'email' => 'test@test.te',
            'password' => 'tester',
            'password_confirmation' => 'tester'
        ]);
        $response
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'email' => [
                        'The email has already been taken.'
                    ]
                ]
            ]);
    }
}
