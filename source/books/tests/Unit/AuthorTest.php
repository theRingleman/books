<?php

namespace Tests\Unit;

use App\Author;
use App\Book;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthorTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        Artisan::call('migrate');
    }

    public function tearDown(){
        Artisan::call('migrate:reset');
        parent::tearDown();
    }

    public function testFindOrCreate()
    {
        $author = factory(Author::class)->create(['first_name' => 'Steven', 'last_name' => 'King']);
        $fetched = Author::findOrCreate('Steven King');
        $this->assertEquals($author->id, $fetched->id, 'We want to make sure that the author being returned already existed in the database.');
    }

    public function testFindOrCreateCreatesNewAuthor()
    {
        factory(Author::class, 12)->create();
        $fetched = Author::findOrCreate('Steven King');
        $this->assertTrue($fetched->wasRecentlyCreated, 'We want to make sure that the author was created by this method.');
    }

    public function testBooks()
    {
        $author = factory(Author::class)->create();
        $books = factory(Book::class, 15)->create()->each(function ($book) use ($author) {
            return $author->books()->save($book);
        });
        $this->assertCount($author->books(), 15);
    }
}
