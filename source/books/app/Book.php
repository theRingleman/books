<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Book
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book query()
 * @mixin \Eloquent
 * @property int author_id
 * @property int id
 * @property string description
 * @property string title
 */
class Book extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['description', 'title', 'publication_date'];
    /**
     * @var array
     */
    protected $hidden = ['author_id'];
    /**
     * @var array
     */
    protected $with = ['author'];

    /**
     * @return mixed
     */
    public function author()
    {
        return $this->belongsTo('App\Author');
    }
}
