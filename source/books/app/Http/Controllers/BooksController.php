<?php

namespace App\Http\Controllers;

use App\Author;
use App\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

/**
 * Class BooksController
 * @package App\Http\Controllers
 */
class BooksController extends Controller
{
    private $validator;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Book::paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $data = $this->getRequestData($request);
        $rules = [
            'title' => 'required|string',
            'description' => 'string',
            'publication_date' => 'string',
            'author' => 'required|string'
        ];
        if ($this->validateData($data, $rules)) {
            return Response::json($this->createBook($data), 200);
        } else {
            return Response::json($this->validator->errors()->all(), 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        return response()->json($book);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Book $book)
    {
        if ($book->user_id === \Auth::user()->id) {
            $data = $this->getRequestData($request);
            $rules = [
                'title' => 'required|string',
                'description' => 'string',
                'publication_date' => 'string'
            ];
            if ($this->validateData($data, $rules)) {
                $book->fill($data);
                if (array_key_exists( 'author', $data)) {
                    $author = Author::findOrCreate($data['author']);
                    $book->author_id = $author->id;
                }
                $book->update();
                return Response::json($book, 200);
            } else {
                return Response::json($this->validator->errors()->all(), 422);
            }
        } else {
            return Response::json(['You must be the creator of this book to edit it.'], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Book $book)
    {
        if ($book->user_id === \Auth::user()->id) {
            $book->delete();
            return Response::json('Book deleted successfully.', 200);
        } else {
            return Response::json('You must be the creator of this book to delete it.', 400);
        }
    }

    /**
     * @param Request $request
     * @return Book
     */
    private function createBook(array $requestData): Book
    {
        $bookModel = new Book();
        $bookModel->fill($requestData);
        $author = Author::findOrCreate($requestData['author']);
        $bookModel->author_id = $author->id;
        $bookModel->user_id = \Auth::user()->id;
        $bookModel->save();
        return $bookModel;
    }

    private function getRequestData(Request $request)
    {
       return json_decode($request->getContent(), true);
    }

    private function validateData(array $requestData, array $rules): bool
    {
        $this->validator = \Validator::make($requestData, $rules);
        return $this->validator->passes() ? true : false;
    }
}
