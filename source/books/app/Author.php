<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Author
 *
 * @package App
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Author newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Author newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Author query()
 * @mixin \Eloquent
 */
class Author extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['first_name', 'last_name'];

    public function books()
    {
        return $this->hasMany('App/Book');
    }

    /**
     * @param string $name
     * @return Author
     */
    public static function findOrCreate(string $name): Author
    {
        [$first, $last] = explode(" ", $name);
        $author = self::where([
            'first_name' => $first,
            'last_name' => $last
        ])->get();

        if (!$author->isEmpty()) {
            return $author->first();
        } else {
            $author = new self();
            $author->first_name = $first;
            $author->last_name = $last;
            $author->save();
            return $author;
        }
    }
}
