<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'ApiAuthController@login');
    Route::post('signup', 'ApiAuthController@signup');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'ApiAuthController@logout');
        Route::get('user', 'ApiAuthController@user');
    });
});

Route::middleware('auth:api')->post('books', 'BooksController@store');
Route::middleware('auth:api')->put('books/{book}', 'BooksController@update');
Route::middleware('auth:api')->delete('books/{book}', 'BooksController@destroy');
Route::get('books', 'BooksController@index');
Route::get('books/{book}', 'BooksController@show');
