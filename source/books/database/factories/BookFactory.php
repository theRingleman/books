<?php

use Faker\Generator as Faker;

$factory->define(App\Book::class, function (Faker $faker) {
    return [
        'title' => $faker->words(2, true),
        'description' => $faker->words(15, true),
        'publication_date' => $faker->date(),
    ];
});
