#!/bin/bash

mkdir docker/source
bindfs -n source/books docker/source

docker build -t booksapi docker

fusermount -u docker/source
rmdir docker/source
